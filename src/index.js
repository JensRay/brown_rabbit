import data from "./data.json";
import "./index.css";
import Mark from "mark.js/dist/mark";

// IMPORT DATA JSON
const articles = data;

// DATE FORMAT FOR ARTICLE.DATE => ARTICLEGENERATOR
function dateFormat(articleDate) {
  const date = new Date(articleDate);
  const day = date.getDate();
  const month = date.getMonth();
  const year = date.getFullYear();
  return day + "/" + month + "-" + year;
}

// TOGGLE BUTTON TO SHORTEN/LENGTHEN ARTICLE.CONTENT
window.onClickReadMore = function (articleId) {
  document.getElementById(articleId).classList.toggle("short");
};

// HIGHLIGHT SEARCH TEXT
const markInstance = new Mark(document.querySelector("#box"));
// Cache DOM elements
let keywordInput = document.querySelector("input[name='keyword']");

window.performMark = function () {
  // Read the keyword
  let keyword = keywordInput.value;

  // Remove previous marked elements and mark
  // the new keyword inside the context
  markInstance.unmark({
    done: function () {
      markInstance.mark(keyword, {});
    },
  });
};

// Listen to input and option changes
keywordInput.addEventListener("input", performMark);

// -------------------------------------------
// PAGINATION

function Pagination() {
  const objJson = data;

  const prevButton = document.getElementById("button_prev");
  const nextButton = document.getElementById("button_next");
  const clickPageNumber = document.querySelectorAll(".clickPageNumber");
  const page_count_move = document.getElementById("page");

  let current_page = 1;
  let records_per_page = 3;
  let page_count = Math.ceil(articles.length / records_per_page);

  this.init = function () {
    changePage(1);
    pageNumbers();
    selectedPage();
    clickPage();
    addEventListeners();
  };

  let addEventListeners = function () {
    prevButton.addEventListener("click", prevPage);
    nextButton.addEventListener("click", nextPage);
  };

  let selectedPage = function () {
    let page_number = document
      .getElementById("page_number")
      .getElementsByClassName("clickPageNumber");
    for (let i = 0; i < page_number.length; i++) {
      if (i == current_page - 1) {
        page_number[i].style.opacity = "1.0";
      } else {
        page_number[i].style.opacity = "0.5";
      }
    }
  };

  let checkButtonOpacity = function () {
    current_page == 1
      ? prevButton.classList.add("opacity")
      : prevButton.classList.remove("opacity");
    current_page == numPages()
      ? nextButton.classList.add("opacity")
      : nextButton.classList.remove("opacity");
  };

  let changePage = function (page) {
    const listingTable = document.getElementById("article");

    if (page < 1) {
      page = 1;
    }
    if (page > numPages() - 1) {
      page = numPages();
    }

    if (page == 1) {
      prevButton.style.visibility = "hidden";
      page_count_move.classList.add("page-count-move");
    } else {
      prevButton.style.visibility = "visible";
      page_count_move.classList.remove("page-count-move");
    }

    if (page == page_count) {
      nextButton.style.visibility = "hidden";
    } else {
      nextButton.style.visibility = "visible";
    }

    listingTable.innerHTML = "";

    articles
      .slice(
        (page - 1) * records_per_page,
        (current_page - 1) * records_per_page + records_per_page
      )
      .map((article) => {
        listingTable.innerHTML += articleGenerator(article);
        page_count_move.innerHTML = `Page ${page} of ${page_count}`;
      });

    function articleGenerator({ id, photoURL, title, date, content }) {
      return `<article id='${id}' class="card card-art d-block d-sm-flex short" >
    <img src="${photoURL}" class="card-img-top" alt="...">
    <div class="card-body py-0 text-sm-start mt-2 mt-sm-0" >
    <h4 class="card-title mb-0 text-start">${title}</h4>
    <h6 class="card-subtitle mb-2 text-start">Posted: ${dateFormat(date)}</h6>
    <p class="card-text text-start" >${content}</p>
    <a class="btn" onclick="onClickReadMore('${id}')" ><span class='label'>&nbsp;</span></a>
    </div>
    </article>`;
    }

    checkButtonOpacity();
    selectedPage();
  };

  let prevPage = function () {
    if (current_page > 1) {
      current_page--;
      changePage(current_page);
    }
  };

  let nextPage = function () {
    if (current_page < numPages()) {
      current_page++;
      changePage(current_page);
    }
  };

  let clickPage = function () {
    document.addEventListener("click", function (e) {
      if (
        e.target.nodeName == "BUTTON" &&
        e.target.classList.contains("clickPageNumber")
      ) {
        current_page = e.target.textContent;
        changePage(current_page);
      }
    });
  };

  let pageNumbers = function () {
    let pageNumber = document.getElementById("page_number");
    pageNumber.innerHTML = "";

    for (let i = 1; i < numPages() + 1; i++) {
      pageNumber.innerHTML +=
        "<button class='clickPageNumber'>" + i + "</button>";
    }
  };

  let numPages = function () {
    return Math.ceil(objJson.length / records_per_page);
  };
}
let pagination = new Pagination();
pagination.init();
