Brown Rabbit
============

This is the compiled job test for a web developer at HTML24 by Jedrzej Lagodzinski.

Instalation
-----------

The website is created with help of webpack.

To install required resources tape in the terminal:
```
npm run build
npm start
```

Content
-------

I've used:
* Bootstrap 5
* Font Awesome
* Webpack
* Mark.js